# enet-js

Modern Node.js bindings for [ENet](http://enet.bespin.org/), the reliable UDP
networking library.

This package uses N-API to provide a foreign function interface for the
native C library

Note that some ENet functions have not been covered yet, so feel free to
contribute the ones you need
